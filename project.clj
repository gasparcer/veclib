(defproject org.clojars.gasparcern "1.0.0"
  :description "A small library for creating 2D mathematical vectors in clojure"
  :url "https://gitlab.com/gasparcer/veclib"
  :license {:name "MIT"
            :url "https://mit-license.org/"}
  :plugins [[cider/cider-nrepl "0.24.0"]
            [lein-expectations "0.0.8"]]
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [expectations "2.1.10"]]
  :deploy-repositories {"releases" {:url "https://repo.clojars.org"}}
  :repl-options {:init-ns veclib.core})
