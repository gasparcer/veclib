(ns veclib.core)

(def pi (Math/PI))
(def e (Math/E))
(defn to-radians [a]
  (Math/toRadians a))
(defn to-degrees [a]
  (Math/toDegrees a))

(defn create-vector [x y]
  {:x x
   :y y
   :vec [x y]})
(defn add [v1 v2]
  (let [vect (vec
               (for [[x y] (map list (:vec v1) (:vec v2))]
                 (+ x y)))]
    {:x (get vect 0)
     :y (get vect 1)
     :vec vect}))

(defn sub [v1 v2]
  (let [vect (vec
               (for [[x y] (map list (:vec v1) (:vec v2))]
                 (- x y)))]
    {:x (get vect 0)
     :y (get vect 1)
     :vec vect}))

(defn dot [v1 v2]
  (let [vect1 (:vec v1)
        vect2 (:vec v2)]
    (reduce + (for [[x y] (map list vect1 vect2)] 
                (* x y)))))
(defn mul [v s]
  (let [vect (:vec v)]
    (vec (for [x vect] 
      (* x s)))))

(defn length [v]
  (Math/sqrt (reduce + (map #(Math/pow % 2) (:vec v)))))

(defn v-from-angle 
  ([a] (let [x (Math/cos a)
             y (Math/sin a)]
         {:x x
          :y y
          :vec [x y]}))
  ([a len] (let [x (* len (Math/cos a))
                 y (* len (Math/sin a))]
             {:x x
              :y y
              :vec [x y]})))
