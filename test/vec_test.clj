(ns vec-test
  (:require [expectations :refer :all]
            [veclib.core :refer :all]))

(expect {:x 2
         :y 3
         :vec [2 3]} (create-vector 2 3))

(expect {:x -3
         :y -9
         :vec [-3 -9]} (create-vector -3 -9))

(expect {:x 3.5355339059327378,
         :y 3.5355339059327373,
         :vec [3.5355339059327378 3.5355339059327373]}
        (v-from-angle (/ pi 4) 5))

